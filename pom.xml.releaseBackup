<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>org.springframework</groupId>
	<artifactId>msa-example</artifactId>
	<version>0.1.1-SNAPSHOT</version>


	<scm>
	    <connection>scm:git:https://bitbucket.org:ivanfontanals/msa-sample.git</connection>
	    <developerConnection>scm:git:git@bitbucket.org:ivanfontanals/msa-sample.git</developerConnection>
	    <url>https://bitbucket.org/ivanfontanals/msa-sample</url>
	    
  </scm>


	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.4.3.RELEASE</version>
	</parent>

	<properties>
		<java.version>1.8</java.version>
		<sonar.language>java</sonar.language>
		<sonar.sourceEncoding>UTF-8</sonar.sourceEncoding>
		<sonar.java.coveragePlugin>jacoco</sonar.java.coveragePlugin>
		<sonar-jacoco-listeners.version>1.4</sonar-jacoco-listeners.version>
		<jacoco.outputDir>${project.build.directory}</jacoco.outputDir>
		<!-- Jacoco output file for UTs -->
		<jacoco.out.ut.file>jacoco-ut.exec</jacoco.out.ut.file>
		<!-- Tells Sonar where the Jacoco coverage result file is located for Unit 
			Tests -->
		<sonar.jacoco.reportPath>${jacoco.outputDir}/${jacoco.out.ut.file}</sonar.jacoco.reportPath>
		<!-- Jacoco output file for ITs -->
		<jacoco.out.it.file>jacoco-it.exec</jacoco.out.it.file>
		<!-- Tells Sonar where the Jacoco coverage result file is located for Integration 
			Tests -->
		<sonar.jacoco.itReportPath>${jacoco.outputDir}/${jacoco.out.it.file}</sonar.jacoco.itReportPath>
	</properties>


	
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-rest</artifactId>
		</dependency>


		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>


		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>


		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>


		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
		</dependency>

		<dependency>
			<groupId>org.codehaus.sonar-plugins.java</groupId>
			<artifactId>sonar-jacoco-listeners</artifactId>
			<version>3.2</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<build>
		<plugins>

			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>


			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<argLine>${jacoco.agent.ut.arg}</argLine>
					<!-- Specific to generate mapping between tests and covered code -->
					<properties>
						<property>
							<name>listener</name>
							<value>org.sonar.java.jacoco.JUnitListener</value>
						</property>
					</properties>
					<!-- test failure ignore -->
					<testFailureIgnore>true</testFailureIgnore>
				</configuration>
			</plugin>


			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-failsafe-plugin</artifactId>
				<configuration>
					<argLine>-Xmx1024m -XX:MaxPermSize=256m ${jacoco.agent.it.arg}
					</argLine>
					<!-- Specific to generate mapping between tests and covered code -->
					<properties>
						<property>
							<name>listener</name>
							<value>org.sonar.java.jacoco.JUnitListener</value>
						</property>
					</properties>
					<!-- Let's put failsafe reports with surefire to have access to tests 
						failures/success reports in sonar -->
					<reportsDirectory>${project.build.directory}/surefire-reports
					</reportsDirectory>
				</configuration>
			</plugin>


			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.7.8</version>
				<executions>
					<!-- Prepares a variable, jacoco.agent.ut.arg, that contains the info 
						to be passed to the JVM hosting the code being tested. -->
					<execution>
						<id>prepare-ut-agent</id>
						<phase>process-test-classes</phase>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
						<configuration>
							<destFile>${sonar.jacoco.reportPath}</destFile>
							<propertyName>jacoco.agent.ut.arg</propertyName>
							<append>true</append>
						</configuration>
					</execution>
					<!-- Prepares a variable, jacoco.agent.it.arg, that contains the info 
						to be passed to the JVM hosting the code being tested. -->
					<execution>
						<id>prepare-it-agent</id>
						<phase>pre-integration-test</phase>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
						<configuration>
							<destFile>${sonar.jacoco.itReportPath}</destFile>
							<propertyName>jacoco.agent.it.arg</propertyName>
							<append>true</append>
						</configuration>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>

</project>
