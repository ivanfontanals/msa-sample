package com.gft.example.test1.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DummyServiceImplTest {

	@Autowired
	private DummyService dummyService;

	@Test
	public void testGetTransactionById() {
		assertThat( dummyService.getTransactionById("TEST_ID") ).isNotEmpty();
		
	}


	
}
