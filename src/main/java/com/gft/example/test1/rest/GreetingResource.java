package com.gft.example.test1.rest;

import java.security.Principal;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gft.example.test1.model.Greeting;
import com.gft.example.test1.model.Transaction;
import com.gft.example.test1.service.DummyService;
import com.gft.example.test1.service.GreetingService;

@RestController
public class GreetingResource implements GreetingService {

	private static Logger log = LoggerFactory.getLogger(GreetingResource.class);

	private static final String TEMPLATE = "Hello, %s!";

	@Autowired
	private DummyService service;

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();
	
	
	@Override
	public String helloWorld(Principal principal) {
		return principal == null ? "Hello anonymous" : "Hello " + principal.getName();
	}

	@Override
	public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }


	@Override
	public HttpEntity<String> echo(
			@RequestParam(value = "text", required = false, defaultValue = "World") String name) {
		log.info("Handling echo");
		return new ResponseEntity<String>(name, HttpStatus.OK);
	}

	@Override
	public Transaction getTransaction() {
		log.info("Handling transaction");
		String id = service.getTransactionById("test");
		Transaction tr = new Transaction();
		tr.setAccount(id);
		tr.setIbanTo("Iban To");
		tr.setTransactionId("ASASDFASDFASDSADFAS");
		return tr;
	}

}