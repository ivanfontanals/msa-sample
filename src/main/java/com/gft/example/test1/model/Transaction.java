package com.gft.example.test1.model;

import lombok.Data;

@Data
public class Transaction {

	private String account;
	private String ibanFrom;
	private String ibanTo;
	private String transactionId;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getIbanFrom() {
		return ibanFrom;
	}

	public void setIbanFrom(String ibanFrom) {
		this.ibanFrom = ibanFrom;
	}

	public String getIbanTo() {
		return ibanTo;
	}

	public void setIbanTo(String ibanTo) {
		this.ibanTo = ibanTo;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

}
