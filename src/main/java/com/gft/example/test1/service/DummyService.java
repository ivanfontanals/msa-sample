package com.gft.example.test1.service;

@FunctionalInterface
public interface DummyService {

	public String getTransactionById(final String id);

}
