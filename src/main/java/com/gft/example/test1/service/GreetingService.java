package com.gft.example.test1.service;

import java.security.Principal;

import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gft.example.test1.model.Greeting;
import com.gft.example.test1.model.Transaction;


public interface GreetingService {

	@RequestMapping(path = "/user", method = RequestMethod.GET)
	@ResponseBody
	public String helloWorld(Principal principal);

	
	@RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name);

	/**
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping(path = "/echo", method = RequestMethod.GET)
	HttpEntity<String> echo(String name);

	/**
	 * 
	 * @return
	 */
	@RequestMapping(path = "/transaction", method = RequestMethod.GET)
	Transaction getTransaction();

}
